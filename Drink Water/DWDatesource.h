//
//  DWDatesource.h
//  Drink Water
//
//  Created by 詹益茂 on 2013/11/28.
//
//

#import <Foundation/Foundation.h>

@interface DWDatesource : NSObject

@property (strong, nonatomic) NSMutableArray *data;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (id) init;
+ (DWDatesource *)sharedDataSource;
- (NSMutableArray *)dataarray;
- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
- (void) saveNewCheckPoint;

@end
