//
//  DWAppDelegate.h
//  Drink Water
//
//  Created by 詹益茂 on 2013/11/28.
//
//

#import <UIKit/UIKit.h>

@interface DWAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
