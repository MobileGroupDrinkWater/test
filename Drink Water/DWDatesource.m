//
//  DWDatesource.m
//  Drink Water
//
//  Created by 詹益茂 on 2013/11/28.
//
//  hello我是王權

#import "DWDatesource.h"
#import <CoreData/CoreData.h>
//負責coredata

#define COREDATA_KEYFORDATE @"date"
#define COREDATA_KEYFORWEIGHT @"weight"

@implementation DWDatesource
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (id)init
{
//    self.data = [[NSMutableArray alloc] initWithArray:@[@10,@9,@8,@7,@6,@5,@4,@3,@2,@1]];
    return  self;
}

+ (DWDatesource *)sharedDataSource
{
    static DWDatesource *sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (NSMutableArray *)dataarray
{
    return self.data;
}

#pragma mark Save a CheckPoint

- (void)saveNewCheckPoint{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSEntityDescription *checkPointEntityDescription = [NSEntityDescription entityForName:@"CheckPoint" inManagedObjectContext:context];
    
    NSManagedObject *newCheckPoint = [[NSManagedObject alloc] initWithEntity:checkPointEntityDescription insertIntoManagedObjectContext:context];
    [newCheckPoint setValue:@"10" forKey:COREDATA_KEYFORWEIGHT];
    [newCheckPoint setValue:@"50" forKey:COREDATA_KEYFORDATE];
    
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

#pragma mark Fetch CheckPoint

- (void)fetchCheckPoint{
    
}

@end
