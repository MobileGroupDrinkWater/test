//
//  main.m
//  Drink Water
//
//  Created by 詹益茂 on 2013/11/28.
//
//

#import <UIKit/UIKit.h>

#import "DWAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([DWAppDelegate class]));
    }
}
