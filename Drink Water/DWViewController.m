//
//  DWViewController.m
//  Drink Water
//
//  Created by 詹益茂 on 2013/11/28.
//
//

#import "DWViewController.h"
#import "DWDatesource.h"

@interface DWViewController ()

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@end

@implementation DWViewController

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;


- (void)viewDidLoad
{
    
//    NSManagedObjectContext *context = [self managedObjectContext];
//
//    NSEntityDescription *authorEntityDescription =
//          [NSEntityDescription entityForName:@"CheckPoint" inManagedObjectContext:context];
//    
//        // Let's put authors
//        NSManagedObject *checkpoint1 = [[NSManagedObject alloc]
//                                    initWithEntity:authorEntityDescription
//                                    insertIntoManagedObjectContext:context];
//        [checkpoint1 setValue:@"2013" forKey:@"date"];
//        [checkpoint1 setValue:@"10" forKey:@"weight"];
//    
//    [self saveContext];
//    
//    
//    NSLog(@"%@",[[DWDatesource sharedDataSource] data]);

    //self.volume.text = [[DWDatesource sharedDataSource] data[1]];
    
    
    [super viewDidLoad];
    [[DWDatesource sharedDataSource]saveNewCheckPoint];
	// Do any additional setup after loading the view, typically from a nib.
}

//- (void)saveContext
//{
//    NSError *error = nil;
//    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
//    if (managedObjectContext != nil) {
//        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
//            // Replace this implementation with code to handle the error appropriately.
//            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
//            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
//            abort();
//        }
//    }
//}
// Returns the URL to the application's Documents directory.
//- (NSURL *)applicationDocumentsDirectory
//{
//    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
//}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
