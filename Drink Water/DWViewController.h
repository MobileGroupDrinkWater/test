//
//  DWViewController.h
//  Drink Water
//
//  Created by 詹益茂 on 2013/11/28.
//
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>


@interface DWViewController : UIViewController

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
@property (strong, nonatomic) IBOutlet UILabel *volume;

@end
